export default {
  name: 'PyRow',
  componentName: 'PyRow',

  props: {
    tag: {
      type: String,
      default: 'div'
    },
    gap: Number,
    type: String,
    justify: {
      type: String,
      default: 'start'
    },
    align: {
      type: String,
      default: 'top'
    }
  },

  computed: {
    style() {
      var ret = {}
      if (this.gap) {
        ret.paddingLeft = `-${this.gap / 2}px`
        ret.paddingRgiht = ret.paddingLeft
      }
      return ret
    }
  },
  render(h) {
    return h(this.tag, {
      class: [
        'py-row',
        this.justify !== 'start' ? `is-justify-${this.justify}` : '',
        this.align !== 'top' ? `is-align-${this.align}` : '',
        {
          'py-row--flex': this.type === 'flex'
        }
      ],
      style: this.style,
    }, this.$slots.default)
  }

}