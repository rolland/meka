import PyBreadcrumb from './src/breadcrumb.vue'

PyBreadcrumb.install = function (Vue) {
  Vue.component(PyBreadcrumb.name, PyBreadcrumb)
}

export default PyBreadcrumb