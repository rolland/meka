import PyButtonGroup from '../button/src/buttonGroup.vue'

PyButtonGroup.install = function (Vue) {
    Vue.component(PyButtonGroup.name, PyButtonGroup)
}

export default PyButtonGroup