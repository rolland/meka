import Vue from 'vue'

//import echarts
import echarts from "echarts/lib/echarts";
import "echarts/lib/chart/pie";
import "echarts/lib/chart/line";
import "echarts/lib/chart/bar";
import "echarts/lib/component/title";
import "echarts/lib/component/tooltip";
import "echarts/lib/component/toolbox";
import "echarts/lib/component/legend";
import "echarts/lib/component/markLine";
import "zrender/lib/svg/svg";





import PyButton from './button/index'
import PyButtonGroup from './button-group/index'
import PyCol from './col/index'
import PyRow from './row/index'
import PyTag from './tag/index'
import Message from './message/index'
import PyBreadcrumb from './breadcrumb/index'
import PyBreadcrumbItem from './breadcrumb-item/index'
import PyCard from './card/index'
import towTable from './two-dimensional-table/index';
import Hour from './hour/index';
import PyIndexcard from './indexcard/index';
import PyLineChart from './charts/linechart/index'
import PyBarChart from './charts/barchart/index'
import PyGEChart from './charts/gechart/index'
import PyRingChart from './charts/ringchart/index'


const components = [
  PyButton,
  PyButtonGroup,
  PyCol,
  PyRow,
  PyTag,
  PyBreadcrumb,
  PyBreadcrumbItem,
  PyCard,
  towTable,
  Hour,
  PyIndexcard,
  PyLineChart,
  PyBarChart,
  PyGEChart,
  PyRingChart

]


const install = function (Vue) {
  if (install.installed) return;
  components.map(component => Vue.component(component.name, component));
  Vue.prototype.$message = Message;
  Vue.prototype.$echarts = echarts;
}


export default {
  install,
  PyButton,
  PyButtonGroup,
  PyCol,
  PyRow,
  PyTag,
  PyBreadcrumb,
  PyBreadcrumbItem,
  PyCard,
  towTable,
  Hour,
  PyIndexcard,
  PyLineChart,
  PyBarChart,
  PyGEChart,
  PyRingChart
}