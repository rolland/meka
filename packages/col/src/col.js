export default {
  name: 'PyCol',

  props: {
    span: {
      type: Number,
      default: 24
    },
    tag: {
      type: String,
      default: 'div'
    },
    offset: Number,
    pull: Number,
    push: Number,
    xs: [Number, Object],
    sm: [Number, Object],
    md: [Number, Object],
    lg: [Number, Object]
  },
  computed: {
    gap() {
      let parent = this.$parent
      while (parent && parent.$options.componentName !== 'PyRow') {
        parent = parent.$parent
      }
      return parent ? parent.gap : 0
    }
  },
  render(h) {
    let classList = []
    let style = {}

    if (this.gap) {
      style.paddingLeft = this.gap / 2 + 'px'
      style.paddingRight = style.paddingLeft
    }
    let attrList = ['span', 'offset', 'pull', 'push']
    let siezList = ['xs', 'sm', 'md', 'lg']
    attrList.forEach(prop => {
      if (this[prop]) {
        classList.push(
          prop !== 'span' ? `py-col-${prop}-${this[prop]}` :
          `py-col-${this[prop]}`
        )
      }
    })

    siezList.forEach(size => {
      if (typeof this[size] === 'number') {
        classList.push(`w-col-${size}-${this[size]}`)
      } else if (typeof this[size] === 'object') {
        let props = this[size]
        Object.keys(props).forEach(prop => {
          classList.push(
            prop !== 'span' ?
            `py-col-${size}-${prop}-${props[prop]}` :
            `py-col-${size}-${props[prop]}`
          )
        })
      }
    })

    return h(this.tag, {
      class: ['py-col', classList],
      style
    }, this.$slots.default)
  }
}