import PyIndexcard from './src/indexcard'

PyIndexcard.install = function (Vue) {
    Vue.component(PyIndexcard.name, PyIndexcard)
}

export default PyIndexcard