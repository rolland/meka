import Table from './src/table.vue.vue'
Table.install = function (Vue) {
  Vue.component(Table.name, Table)
}

export default Table