import PyRingChart from './src/ringchart'

PyRingChart.install = function (Vue) {
    Vue.component(PyRingChart.name, PyRingChart)
}

export default PyRingChart