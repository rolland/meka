import PyBarChart from './src/barchart'

PyBarChart.install = function (Vue) {
    Vue.component(PyBarChart.name, PyBarChart)
}

export default PyBarChart