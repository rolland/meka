import PyLineChart from './src/linechart'

PyLineChart.install = function (Vue) {
    Vue.component(PyLineChart.name, PyLineChart)
}

export default PyLineChart