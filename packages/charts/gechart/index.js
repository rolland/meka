import PyGEChart from './src/gechart'

PyGEChart.install = function (Vue) {
    Vue.component(PyGEChart.name, PyGEChart)
}

export default PyGEChart