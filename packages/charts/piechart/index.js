import PyPieChart from './src/piechart'

PyPieChart.install = function (Vue) {
    Vue.component(PyPieChart.name, PyPieChart)
}

export default PyPieChart