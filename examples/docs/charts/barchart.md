# PyBarchart 线图

---

### 基础用法

使用`vaxis` 属性来定义 py-barchart 的样式

<div class="demo-box">
  <div class="demo-block">
    <py-row id="lineRow">
      <py-col :span="12">
        <py-barchart id="line1" :data="binddata"></py-barchart>
      </py-col>
      <py-col :span="12">
        <py-barchart id="line2" :data="binddata" vaxis></py-barchart>
      </py-col>
    </py-row>
  </div>
</div>

<script>
export default {
  data() {
    return {
      binddata: {
        value:[[820, 932, 901, 934, 1290, 1330, 1320]],
        axis:['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      }
    };
  }
}
</script>

<style scoped>
  #lineRow {
     width: 100%;
     height: 400px;
 }
 #lineRow .py-col{
   height:100%;
 }
 #line1{
   height:100%;
 }
 #line2{
   height:100%;
 }
</style>

::: demo

```html
<div class="demo-box">
  <div class="demo-block">
    <py-row id="lineRow">
      <py-col :span="12">
        <py-barchart id="line1" :data="binddata"></py-barchart>
      </py-col>
      <py-col :span="12">
        <py-barchart id="line2" :data="binddata" area smooth></py-barchart>
      </py-col>
    </py-row>
  </div>
</div>

  <script>
    export default {
      data() {
        return {
          binddata: {
            value: [[820, 932, 901, 934, 1290, 1330, 1320]],
            axis: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
          }
        };
      }
    };
  </script>
  <style scoped>
  #lineRow {
     width: 100%;
     height: 400px;
 }
 #lineRow .py-col{
   height:100%;
 }
  </style>
</div>
```

:::

</div>

### 其他元素设置

#### 设置标题

#### 设置颜色

### 设置多条线

### Attributes

| 参数   | 说明               | 类型    | 可选值 | 默认值 |
| ------ | ------------------ | ------- | ------ | ------ |
| title  | 标题               | string  | —      | —      |
| area   | 是否显示面积       | Boolean | —      | false  |
| smooth | 是否显示为平滑曲线 | Boolean | —      | false  |
| vaxis  | 是否翻转坐标轴     | Boolean | —      | false  |
