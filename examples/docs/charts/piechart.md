# PyLineChart 线图

---

### 基础用法

使用`area`, `smooth`和`vaxis` 属性来定义 py-linechart 的样式
`area` 设置显示面积区域
`smooth` 设置显示为平滑曲线
`vaxis` 表示翻转坐标轴

<div class="demo-box">
  <div class="demo-block">
    <py-row id="lineRow">
      <py-col :span="12">
        <py-linechart id="line1" :data="binddata"></py-linechart>
      </py-col>
      <py-col :span="12">
        <py-linechart id="line2" :data="binddata" area smooth></py-linechart>
      </py-col>
    </py-row>
  </div>
</div>

<script>
export default {
  data() {
    return {
      binddata: {
        value:[[820, 932, 901, 934, 1290, 1330, 1320]],
        axis:['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      }
    };
  }
}
</script>

<style scoped>
  #lineRow {
     width: 100%;
     height: 400px;
 }
 #lineRow .py-col{
   height:100%;
 }
 #line1{
   height:100%;
 }
 #line2{
   height:100%;
 }
</style>

::: demo

```html
<div class="demo-box">
  <div class="demo-block">
    <py-row id="lineRow">
      <py-col :span="12">
        <py-linechart id="line1" :data="binddata"></py-linechart>
      </py-col>
      <py-col :span="12">
        <py-linechart id="line2" :data="binddata" area smooth></py-linechart>
      </py-col>
    </py-row>
  </div>
</div>

  <script>
    export default {
      data() {
        return {
          binddata: {
            value: [[820, 932, 901, 934, 1290, 1330, 1320]],
            axis: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
          }
        };
      }
    };
  </script>
  <style scoped>
  #lineRow {
     width: 100%;
     height: 400px;
 }
 #lineRow .py-col{
   height:100%;
 }
  </style>
</div>
```

:::

</div>
<!------------------------------------------------------ -->

### 环形进度图

<div class="demo-box">
  <div class="demo-block">
    <py-row id="lineRow1">
      <py-col :span="8">
        <py-ringchart id="ring1" :value="130" :total='200' :fontSize='50' unit="台" title="可用vCPU"></py-ringchart>
      </py-col>
      <py-col :span="8">
        <py-ringchart id="ring2" :value="45" :total='78' isPer></py-ringchart>
      </py-col>
       <py-col :span="8">
        <py-ringchart id="ring3" :percent="45" isPer></py-ringchart>
      </py-col>
    </py-row>
    <py-row id="lineRow2">
      <py-col :span="4">
        <py-ringchart id="ring1" :value="150" :thres="100" :total='200' :radius="['80%','90%']" unit="G"></py-ringchart>
      </py-col>
      <py-col :span="4">
        <py-ringchart id="ring2" :value="45" :total='78' isPer title="可用内存"></py-ringchart>
      </py-col>
       <py-col :span="4">
        <py-ringchart id="ring3" :percent="45" isPer></py-ringchart>
      </py-col>
       <py-col :span="4">
        <py-ringchart id="ring4" :percent="90"  :thres="80" isPer></py-ringchart>
      </py-col>
    </py-row>
  </div>
</div>

  <style scoped>
  #lineRow1 {
     width: 100%;
     height: 400px;
 }
   #lineRow2 {
     width: 100%;
     height: 200px;
 }
 .py-row .py-col{
   height:100%;
 }
 .chart-container{
   height:100%;
 }
  </style>

::: demo

```html
<div class="demo-box">
  <div class="demo-block">
    <py-row id="lineRow">
      <py-col :span="12">
        <py-linechart id="line1" :data="binddata"></py-linechart>
      </py-col>
      <py-col :span="12">
        <py-linechart id="line2" :data="binddata" area smooth></py-linechart>
      </py-col>
    </py-row>
  </div>
</div>

</div>
```

:::

</div>

### Attributes

| 参数   | 说明               | 类型    | 可选值 | 默认值 |
| ------ | ------------------ | ------- | ------ | ------ |
| title  | 标题               | string  | —      | —      |
| area   | 是否显示面积       | Boolean | —      | false  |
| smooth | 是否显示为平滑曲线 | Boolean | —      | false  |
| vaxis  | 是否翻转坐标轴     | Boolean | —      | false  |
