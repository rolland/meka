# Button 按钮

---

### 基础用法

使用`type`, `plain`和`round` 属性来定义 Button 的样式

<div class="demo-box">
  <div class="demo-block">
    <div>
      <py-button type="default" >默认按钮</py-button>
      <py-button type="primary">主要按钮</py-button>
      <py-button type="success">成功按钮</py-button>
      <py-button type="warning">警告按钮</py-button>
      <py-button type="danger">危险按钮</py-button>
      <py-button type="info">信息按钮</py-button>
    </div>
    <div>
      <py-button type="default" plain>朴素按钮</py-button>
      <py-button type="primary" plain>主要按钮</py-button>
      <py-button type="success" plain>成功按钮</py-button>
      <py-button type="warning" plain>警告按钮</py-button>
      <py-button type="danger" plain>危险按钮</py-button>
      <py-button type="info" plain>信息按钮</py-button>
    </div>
    <div>
      <py-button type="default" round>默认按钮</py-button>
      <py-button type="primary" round>主要按钮</py-button>
      <py-button type="success" round>成功按钮</py-button>
      <py-button type="warning" round>警告按钮</py-button>
      <py-button type="danger" round>危险按钮</py-button>
      <py-button type="info" round>信息按钮</py-button>
    </div>
  </div>

::: demo

```html
<div>
  <py-button type="default">默认按钮</py-button>
  <py-button type="primary">主要按钮</py-button>
  <py-button type="success">成功按钮</py-button>
  <py-button type="warning">警告按钮</py-button>
  <py-button type="danger">危险按钮</py-button>
  <py-button type="info">信息按钮</py-button>
</div>
<div>
  <py-button type="default" plain="true">朴素按钮</py-button>
  <py-button type="primary" plain="true">主要按钮</py-button>
  <py-button type="success" plain="true">成功按钮</py-button>
  <py-button type="warning" plain="true">警告按钮</py-button>
  <py-button type="danger" plain="true">危险按钮</py-button>
  <py-button type="info" plain="true">信息按钮</py-button>
</div>
<div>
  <py-button type="default" round>默认按钮</py-button>
  <py-button type="primary" round>主要按钮</py-button>
  <py-button type="success" round>成功按钮</py-button>
  <py-button type="warning" round>警告按钮</py-button>
  <py-button type="danger" round>危险按钮</py-button>
  <py-button type="info" round>信息按钮</py-button>
</div>
```

:::

</div>

### 图标按钮

带图标的按钮可增强辨识度（有文字）或节省空间（无文字）。使用`icon`属性来定义 Button 的样式

<div class="demo-box">
  <div class="demo-block">
    <div>
      <py-button icon="py-icon-remindfill">默认按钮</py-button>
      <py-button type="primary" icon="py-icon-delete">删除</py-button>
      <py-button type="success" icon="py-icon-home"></py-button>
      <py-button type="warning" icon="py-icon-closefill" size="small">关闭</py-button>
      <py-button type="warning" icon="py-icon-closefill" size="medium">关闭</py-button>
    </div>
  </div>

::: demo

```html
<div>
  <py-button disabled>默认按钮</py-button>
  <py-button type="primary" disabled>主要按钮</py-button>
  <py-button type="success" disabled>成功按钮</py-button>
  <py-button type="info" disabled>信息按钮</py-button>
  <py-button type="warning" disabled>警告按钮</py-button>
  <py-button type="danger" disabled>危险按钮</py-button>
</div>
<div>
  <py-button plain disabled>朴素按钮</py-button>
  <py-button type="primary" plain disabled>主要按钮</py-button>
  <py-button type="success" plain disabled>成功按钮</py-button>
  <py-button type="info" plain disabled>信息按钮</py-button>
  <py-button type="warning" plain disabled>警告按钮</py-button>
  <py-button type="danger" plain disabled>危险按钮</py-button>
</div>
```

:::

</div>

### 禁用状态

按钮不可用状态。

<div class="demo-box">
  <div class="demo-block">
    <div>
      <py-button disabled>默认按钮</py-button>
      <py-button type="primary" disabled>主要按钮</py-button>
      <py-button type="success" disabled>成功按钮</py-button>
      <py-button type="info" disabled>信息按钮</py-button>
      <py-button type="warning" disabled>警告按钮</py-button>
      <py-button type="danger" disabled>危险按钮</py-button>
    </div>
    <div >
      <py-button plain disabled>朴素按钮</py-button>
      <py-button type="primary" plain disabled>主要按钮</py-button>
      <py-button type="success" plain disabled>成功按钮</py-button>
      <py-button type="info" plain disabled>信息按钮</py-button>
      <py-button type="warning" plain disabled>警告按钮</py-button>
      <py-button type="danger" plain disabled>危险按钮</py-button>
    </div>
  </div>

::: demo

```html
<div>
  <py-button disabled>默认按钮</py-button>
  <py-button type="primary" disabled>主要按钮</py-button>
  <py-button type="success" disabled>成功按钮</py-button>
  <py-button type="info" disabled>信息按钮</py-button>
  <py-button type="warning" disabled>警告按钮</py-button>
  <py-button type="danger" disabled>危险按钮</py-button>
</div>
<div>
  <py-button plain disabled>朴素按钮</py-button>
  <py-button type="primary" plain disabled>主要按钮</py-button>
  <py-button type="success" plain disabled>成功按钮</py-button>
  <py-button type="info" plain disabled>信息按钮</py-button>
  <py-button type="warning" plain disabled>警告按钮</py-button>
  <py-button type="danger" plain disabled>危险按钮</py-button>
</div>
```

:::

</div>

### 不同尺寸

Button 组件提供除了默认值以外的三种尺寸，可以在不同场景下选择合适的按钮尺寸。
额外的尺寸：`medium`、`small`，通过设置`size`属性来配置它们。

<div class="demo-box">
  <div class="demo-block">
    <py-button size="small">小尺寸</py-button>
    <py-button size="medium">中等尺寸</py-button>
    <py-button size="default">默认尺寸</py-button>
  </div>

::: demo

```html
<div>
  <py-button size="small">小尺寸</py-button>
  <py-button size="medium">中等尺寸</py-button>
  <py-button size="default">默认尺寸</py-button>
</div>
```

:::

</div>

### 图标组

以按钮组的方式出现，常用于多项类似操作。

<div class="demo-box">
  <div class="demo-block">
    <py-button-group>
      <py-button icon="fa fa-home">默认</py-button>
      <py-button icon="fa fa-trash-alt">删除</py-button>
      <py-button icon="fa fa-ellipsis-h">其他</py-button>
    </py-button-group>
  </div>

  <div class="demo-block">
    <py-button-group>
      <py-button type="chathamsbluelight" size="small" icon="fas fa-edit">编辑视图</py-button>
      <py-button type="chathamsbluelight" size="small" icon="fas fa-cog fa-spin">权限设置</py-button>
      <py-button type="chathamsbluelight" size="small" icon="fas fa-bullseye">资源扫描</py-button>
    </py-button-group>
  </div>

::: demo

```html

<py-button-group>
  <py-button icon="fa fa-home">默认</py-button>
  <py-button icon="fa fa-trash-alt">删除</py-button>
  <py-button icon="fa fa-ellipsis-h">其他</py-button>
</py-button-group>

<py-button-group>
  <py-button type="chathamsbluelight" size="small" icon="fas fa-edit">编辑视图</py-button>
  <py-button type="chathamsbluelight" size="small" icon="fas fa-cog fa-spin">权限设置</py-button>
  <py-button type="chathamsbluelight" size="small" icon="fas fa-bullseye">资源扫描</py-button>
</py-button-group>


```

:::

</div>

### Attributes

| 参数     | 说明                         | 类型    | 可选值                              | 默认值 |
| -------- | ---------------------------- | ------- | ----------------------------------- | ------ |
| size     | 尺寸                         | string  | default,medium,small                | —      |
| type     | 类型                         | string  | primary,success,warning,danger,info | —      |
| plain    | 是否朴素按钮                 | Boolean | —                                   | false  |
| disabled | 是否禁用状态                 | boolean | —                                   | false  |
| icon     | 图标，已有的图标库中的图标名 | string  | —                                   | —      |
