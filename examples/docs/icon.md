<script>
  var iconList = require('../icon.json');

  export default {
    data() {
      return {
        icons: iconList
      };
    }
  }
</script>
<style lang="scss">
  .demo-icon .source > i {
    font-size: 24px;
    color: #8492a6;
    margin: 0 20px;
    font-size: 1.5em;
    vertical-align: middle;
  }
  
  .demo-icon .source > button {
    margin: 0 20px;
  }

  .icon-list {
    overflow: hidden;
    list-style: none;
    padding: 0;
    border: solid 1px #3b6897;
    border-radius: 4px;
  }
  .icon-list li {
    float: left;
    width: 16.66%;
    text-align: center;
    height: 120px;
    line-height: 120px;
    color: #666;
    font-size: 13px;
    transition: color .15s linear;

    border-right: 1px solid #3b6897;
    border-bottom: 1px solid #3b6897;
    margin-right: -1px;
    margin-bottom: -1px;
    span {
      display: inline-block;
      line-height: normal;
      vertical-align: middle;
      font-family: 'Helvetica Neue',Helvetica,'PingFang SC','Hiragino Sans GB','Microsoft YaHei',SimSun,sans-serif;
      color: #99a9bf;
    }
    i {
      display: block;
      font-size: 32px;
      margin-bottom: 15px;
      color: #dee3e9;
    }
    &:hover {
      color: rgb(92, 182, 255);
    }
  }
</style>

# Icon 图标

---

语义化的矢量图形，`meka-ui` 使用开源的 [Font Awesome](https://fontawesome.com/icons?d=gallery) 作为图标库。

### 如何使用

使用 `class="fas fa-*"` 来声明图标，具体图标的名称请 `copy` 相应的标签，更多的图标和使用方法请参考 [Font Awesome「basic-use」](https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use)。

<div class="demo-box">
  <div class="demo-block">
    <i class="fas fa-home fa-2x"></i>
    <i class="fas fa-trash-alt fa-2x"></i>
    <i class="fas fa-edit fa-2x"></i>
    <i class="fas fa-cog fa-2x"></i>
    <i class="fas fa-bullseye fa-2x"></i>
    <i class="fas fa-file-alt fa-2x"></i>
    <i class="fas fa-tv fa-2x"></i>
    <i class="fas fa-star fa-2x fa-spin"></i>
    <i class="fas fa-tags fa-2x"></i>
  </div>

::: demo

```html

    <i class="fas fa-home fa-2x"></i>
    <i class="fas fa-trash-alt fa-2x"></i>
    <i class="fas fa-edit fa-2x"></i>
    <i class="fas fa-cog fa-2x"></i>
    <i class="fas fa-bullseye fa-2x"></i>
    <i class="fas fa-file-alt fa-2x"></i>
    <i class="fas fa-tv fa-2x"></i>
    <i class="fas fa-star fa-2x fa-spin"></i>
    <i class="fas fa-tags fa-2x"></i>

```

:::

</div>

### 图标示例

<ul class="icon-list">
  <li v-for="name in icons" :key="name">
    <span>
      <i :class="'fas ' + name"></i>
      {{'fas ' + name}}
    </span>
  </li>
</ul>
