import Vue from 'vue'
import Vuex from 'vuex'



Vue.use(Vuex)

const types = {

  GET_MENU_LIST: 'GET_MENU_LIST',

}

const state = {

  viewData: {

  },
  menuData: []


}
const mutations = {
  [types.GET_MENU_LIST](state, data) {
    //console.log(data);
    this.state.menuData = data;


  }

}
const actions = {
  getMenuList(context, data) {
    context.commit(types.GET_MENU_LIST, {});
  },

}

const getters = {};


export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})