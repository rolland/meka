// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import demoBlock from "./components/demo-block.vue";
import router from "./router";
import store from "./vuex/store";
import GeminiScrollbar from "vue-gemini-scrollbar";
import MEKA from "../packages/index";

Vue.component("demo-block", demoBlock);

Vue.config.productionTip = false;
Vue.use(GeminiScrollbar);
Vue.use(MEKA);

//css
import "../packages/theme/theme-bmc/lib/index.scss";
import "./assets/css/md.css";
import "../node_modules/highlight.js/styles/color-brewer.css";

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: {
    App
  },
  template: "<App/>"
});